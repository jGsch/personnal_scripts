# Personnal scripts

Various scripts for various uses!

`export PATH=<PATH>/personnal_scripts:"$PATH"` to your `.bashrc` to use this scripts everywhere


## General utilities

> toggle_swap

Empty SWAP to improve speed if slow hard disks and limited RAM.

## Music

> lossless_to_mp3.py

Convert all the lossless files (flac, wav, alac) of a folder in .mp3 files. Add `-cd` or `--create-dir` to save mp3 file in a new directory.

> image_2_files.py

Split all .flac CD images in a folder using associated CUE files. Be sure that `shntool` and `cuetools` packages are installed.


