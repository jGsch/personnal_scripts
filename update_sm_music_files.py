#!/usr/bin/env python3
import os 
import mutagen

pc_path = os.path.expanduser('~') + '/Music/COMPILATIONS'
sm_path = '/storage/BE5C-BFFD/Music'

def list_all_dir(path): 
    return [file for file in os.listdir(path) if os.path.isdir(os.path.join(path, file))] 

# List to update
exclude = ['_HQ', 'Mix', 'Techno_de_base', 'Reggae', 'Musique_de_rue', 
           'Psytrance', 'Minimal', 'Tech_House']

folders  = list_all_dir(pc_path)
folders = [folder for folder in folders if folder not in exclude]

for folder in folders:
    sub_folders = list_all_dir('%s/%s' % (pc_path, folder))
    if sub_folders:
        sub_folders = [f'{folder}/{sub_dir}' for sub_dir in sub_folders if sub_dir not in exclude]
        folders.extend(sub_folders)   
        folders.remove(folder)

# Send files
print('Sending:')

playlist_name = f'{pc_path}/last_tracks_playlist.m3u'
if os.path.exists(playlist_name): 
    with open(playlist_name, 'r') as f:
        playlist = f.readlines()
else:
    playlist = ['#EXTM3U\n']

for folder in folders:
    sm = os.popen(f'adb shell ls "{sm_path}/{folder}"').read().split('\n')
    pc = os.listdir(f'{pc_path}/{folder}')

    # Clean lists
    sm = [f for f in sm if 'mp3' in f]
    pc = [f for f in pc if 'mp3' in f]

    for missing in set(pc).difference(sm):
        # print(' %s/%s' % (folder, missing), end=' > ')
        sm_file = '%s/%s/%s' % (sm_path, folder, missing)
        pc_file = '%s/%s/%s' % (pc_path, folder, missing)

        file = mutagen.File(pc_file) 
        
        playlist.insert(1, f"{sm_file}\n") 
        playlist.insert(1, f"#EXTINF:{int(file.info.length)},{file['TIT2']} - {file['TPE1']} \n") 
        
        os.system(f'adb push "{pc_file}" "{sm_file}"')

with open(playlist_name, 'w') as f:
    for line in playlist[:81]:
        f.write(line)

