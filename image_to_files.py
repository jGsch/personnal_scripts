#!/usr/bin/env python3

import os
import shutil
import argparse
from lossless_to_mp3 import lossless_to_mp3


# Create parser
parser = argparse.ArgumentParser()
parser.add_argument("-ctm", "--convert-to-mp3", help="", action='store_true')
args = parser.parse_args()

# List all files
files = os.listdir()

formats = ['.flac', '.wav', '.cue']
files = [file for file in files if any(format in file for format in formats)]


if len(files) % 2 == 0:

    # Remove type from files
    files = [os.path.splitext(file)[0] for file in files]
    
    # Remove duplicate
    files = list(dict.fromkeys(files))
    
    for file in files:
        print('Processing %s...\n' % file)

        # Create new dir 
        dir = os.getcwd().split('/')[-1] + '_SPLITTED'
        if os.path.isdir(dir):
            shutil.rmtree(dir) 
        os.mkdir(dir)
        
        # Split image file
        cmd = 'shnsplit -O always -t %%n-%%t -o flac -d "%s" -f "%s.cue" "%s.flac"' % (dir, file, file)
        os.system(cmd)

        
        # Convert to mp3 if wanted
        if args.convert_to_mp3:
            lossless_to_mp3('%s/%s' % (os.getcwd(), dir), create_dir=True)
else:
    raise OSError
