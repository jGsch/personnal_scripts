#!/usr/bin/env python3

import os
import argparse 
from pathlib import Path

def lossless_to_mp3(path, create_dir=True):

    if create_dir:
        path.joinpath(f'{path.stem}_MP3').mkdir(exist_ok=True)
        new_dir = path / f'{path.stem}_MP3'

    for f in path.glob('*.flac'):
        print(f'Convert > {str(f)}')

        parameters = '-hide_banner -loglevel panic'
        target =  new_dir / f.stem if create_dir else f'{f.with_suffix("")}'
        os.system(f'ffmpeg {parameters} -i "{str(f)}" -ab 320k "{target}.mp3"')

    print('Convertion done!\n')

if __name__ == '__main__':
    # Create parser
    parser = argparse.ArgumentParser()
    parser.add_argument("-cd", "--create-dir", help="", action='store_true')
    args = parser.parse_args()

    lossless_to_mp3(Path.cwd(), args.create_dir)
