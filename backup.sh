
save () {

    rsync -rtv --exclude=".*" ~/Documents/ /media/jg/$1/PERSONNAL/
    rsync -rtv --delete ~/Music/COMPILATIONS/ /media/jg/$1/MUSIC/COMPILATIONS/
    rsync -rtv --delete ~/.ssh/ /media/jg/$1/PERSONNAL/SSH_KEYS/

}

if [ -d "/media/jg/Untz" ]; then
    echo "\n\n Untz\n\n"
    save "Untz"
fi

if [ -d "/media/jg/data_01" ]; then
    echo "\n\n data_01\n\n"
    save "data_01"
fi
